#'//////////////////////////////////////////////////////////////////////////////
#' FILE: app.R
#' AUTHOR: Fatih Uenal
#' CREATED: 19-03-2021
#' MODIFIED: 19-03-2021
#' PURPOSE: Supply-side policies interactive mapping tool
#' PACKAGES: various, see below
#' COMMENTS: NA
#'//////////////////////////////////////////////////////////////////////////////

## includes code adapted from the following sources:
# https://github.com/eparker12/nCoV_tracker
# https://davidruvolo51.github.io/shinytutorials/tutorials/rmarkdown-shiny/
# https://github.com/rstudio/shiny-examples/blob/master/087-crandash/
# https://rviews.rstudio.com/2019/10/09/building-interactive-world-maps-in-shiny/
# https://github.com/rstudio/shiny-examples/tree/master/063-superzip-example

# ShinyProxy + Docker Turtorial here: https://towardsdatascience.com/an-open-source-solution-to-deploy-enterprise-level-r-shiny-applications-2e19d950ff35

## Data Pre-processing Script
# Pre-processing data for app
# Preprocessing.R
# Creates countries_overview_large, state_city_breakdown_map, oil_production, gas_production, coal_production, policy files

# update data with automated script
# source("divestment_data_daily.R") # option to update weekly new divestment policies 
# source("crowdsourced_data_daily.R") # option to update weekly new manual entry policies
# source("newsAPI_data_weekly.R") # option to update weekly NewsAPI entries 

# load required packages
library(magrittr)
# library(rvest)
# library(stringr)
# library(stringi)
# library(readxl)
library(dplyr)
# library(maps)
library(ggplot2)
# library(reshape2)
# library(ggiraph)
# library(RColorBrewer)
library(leaflet)
library(geojsonio)
library(shiny)
library(shinyWidgets)
library(shinydashboard)
library(shinythemes)
# library(sjmisc)
# library(lubridate)
library(kableExtra)
# library(gridExtra)
# library(shinyjs)

# pkgs
suppressPackageStartupMessages(library(shiny))

# Load Image
# save.image("app/image.RData")
load("image.RData")

