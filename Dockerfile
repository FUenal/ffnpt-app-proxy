FROM rocker/r-ubuntu:20.04

LABEL maintainer="Fatih Uenal <mars.fatih@gmail.com>"

# system libraries of general use
## install debian packages
RUN apt-get update -qq && apt-get -y --no-install-recommends install \
    libxml2-dev \
    libcairo2-dev \
    libsqlite3-dev \
    libpq-dev \
    libssh2-1-dev \
    unixodbc-dev \
    r-cran-v8 \
    libv8-dev \
    net-tools \
    libprotobuf-dev \
    protobuf-compiler \
    libjq-dev \
    libudunits2-0 \
    libudunits2-dev \
    libgdal-dev \
    libssl-dev \
    sudo \
    libcurl4-gnutls-dev \
    libxt-dev \
    && rm -rf /var/lib/apt/lists/*
    
## update system libraries
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get clean
    
    
# install R packages required 
RUN R -e "install.packages(c('magrittr', 'dplyr', 'ggplot2', 'leaflet', 'geojsonio', 'shiny', 'shinydashboard', 'kableExtra', 'shinyWidgets', 'shinythemes'), repos='https://packagemanager.rstudio.com/all/__linux__/focal/latest')"

RUN echo "local(options(shiny.port = 3838, shiny.host = '0.0.0.0'))" > /usr/lib/R/etc/Rprofile.site

RUN addgroup --system app \
    && adduser --system --ingroup app app

WORKDIR /home/app

COPY app .

RUN chown app:app -R /home/app

USER app

EXPOSE 3838

CMD ["R", "-e", "shiny::runApp('/home/app')"]
